<?php

class Template
{
    private $arquivo;
    private $variaveis;
    
    public function _construct(string $arquivo){
        $this->$arquivo = $arquivo;
    } 

    public function setVariaveis(array $vetor): bool{
        $this->$variaveis = $variaveis;
    }

    public function mostrar(){
        include($this->arquivo);
    }
}